import { useState, useEffect } from "react";

// export default () => {
function ShoesList() {
    const [shoes, setShoes] = useState([])

    const url = 'http://localhost:8080/api/shoes/'

    const fetchShoes = async () => {
        const res = await fetch(url)
        const shoesJSON = await res.json()
        setShoes(shoesJSON.shoes);
    }

    const handleDelete = async (id) => {
        const url2 = `${url}${id}`
        const fetchConfig = {
            method: "delete"
        };
        const response = await fetch(url2, fetchConfig);
        setShoes(
            shoes.filter((shoe) => {
                return shoe.id !== id;
            })
        )
    }

    const handleEdit = (id) => {
        const url2 = `${url}${id}`
        window.location.href=url2;
    }

    useEffect(() => {
        fetchShoes()
    }, [])

    return (
        <>
            <button
                className="btn btn-primary"
                onClick={(e) => {
                    e.preventDefault();
                    window.open('http://localhost:3000/shoes/new');
                }}
                >Create shoe
            </button>
            &nbsp;
            <button
                className="btn btn-outline-secondary"
                onClick={(e) => {
                    e.preventDefault();
                    window.open('http://localhost:3000/bins/new');
                }}
                >Create bin
            </button>
            <table className="table">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Picture URL</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => (
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.picture_url}</td>
                            <td>{shoe.bin}</td>
                            <td><button onClick={() => handleEdit(shoe.id)} className="btn btn-outline-warning">Edit</button></td>
                            <td><button onClick={() => handleDelete(shoe.id)} className="btn btn-outline-danger">Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

export default ShoesList;